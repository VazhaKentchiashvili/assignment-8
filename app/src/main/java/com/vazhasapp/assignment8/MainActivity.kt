package com.vazhasapp.assignment8

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.vazhasapp.assignment8.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var myAdapter: MonsterAdapter
    private val monstersDummyData = mutableListOf<MonstersModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_Assignment8)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()

    }

    companion object {
        const val DETAILS_INTENT = "MonsterDetails"
    }

    private fun init() {
        setData()
        myAdapter = MonsterAdapter(monstersDummyData, object : OnMonsterClickListener {
            override fun onClick(position: Int) {
                val mIntent = Intent(this@MainActivity, MonsterDetailsActivity::class.java)
                mIntent.putExtra(DETAILS_INTENT, monstersDummyData[position])
                startActivity(mIntent)
            }

            override fun onLongClick(position: Int) {
                monstersDummyData.removeAt(position)
                myAdapter.notifyItemRemoved(position)
            }
        })
        binding.recyclerView.apply {
            adapter = myAdapter
            layoutManager = GridLayoutManager(this@MainActivity, 2)
            setHasFixedSize(true)
        }
    }

    private fun setData() {
        monstersDummyData.add(
            MonstersModel(
                "ჯიმშერი",
                "ნაპოვნი იქნა ლამისყანაში",
                R.drawable.ic_monster_first
            )
        )
        monstersDummyData.add(MonstersModel("არტური", "იძებნება!"))
        monstersDummyData.add(
            MonstersModel(
                "კიჭა",
                "ნაპოვნი იქნა კასპში",
                R.drawable.ic_monster_fifth
            )
        )
        monstersDummyData.add(MonstersModel("ბჟუტურა", "იძებნება!"))
        monstersDummyData.add(
            MonstersModel(
                "მზექალა",
                "ნაპოვნი იქნა ლაგოდეხში",
                R.drawable.ic_monster_third
            )
        )
        monstersDummyData.add(
            MonstersModel(
                "მურტალო",
                "ნაპოვნი იქნა ინგას სახლში",
                R.drawable.ic_monster_fourth
            )
        )
        monstersDummyData.add(MonstersModel("ესკიმო", "იძებნება!"))
        monstersDummyData.add(MonstersModel("ანგელოზა", "იძებნება!"))
        monstersDummyData.add(
            MonstersModel(
                "ავთო",
                "ნაპოვნი იქნა ბირჟაზე",
                R.drawable.ic_monster_seventh
            )
        )
        monstersDummyData.add(MonstersModel("კარება", "იძებნება!"))
    }
}
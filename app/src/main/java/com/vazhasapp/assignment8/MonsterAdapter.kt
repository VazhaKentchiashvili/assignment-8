package com.vazhasapp.assignment8

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.assignment8.databinding.FindedMonsterCardViewBinding
import com.vazhasapp.assignment8.databinding.NotFindedMonsterCardViewBinding


class MonsterAdapter(
    private val monstersList: MutableList<MonstersModel>,
    private val clickListener: OnMonsterClickListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val FOUNDED_MONSTER = 1
        private const val NOT_FOUNDED_MONSTER = 2
    }

    override fun getItemViewType(position: Int): Int {
        val currentPosition = monstersList[position]
        return if (currentPosition.monsterIcon == null)
            NOT_FOUNDED_MONSTER
        else
            FOUNDED_MONSTER
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == FOUNDED_MONSTER) {
            FoundMonsterViewHolder(
                FindedMonsterCardViewBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        } else {
            NotFoundMonsterViewHolder(
                NotFindedMonsterCardViewBinding.inflate(
                    LayoutInflater.from(
                        parent.context
                    ), parent, false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is FoundMonsterViewHolder -> holder.bind()
            is NotFoundMonsterViewHolder -> holder.bind()
        }
    }

    override fun getItemCount() = monstersList.size

    inner class FoundMonsterViewHolder(private val binding: FindedMonsterCardViewBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener, View.OnLongClickListener {

        private lateinit var currentFoundedMonster: MonstersModel
        fun bind() {
            currentFoundedMonster = monstersList[adapterPosition]
            binding.imIcon.setImageResource(currentFoundedMonster.monsterIcon!!)
            binding.tvTitle.text = currentFoundedMonster.monsterName
            binding.tvDescription.text = currentFoundedMonster.monsterDescription

            binding.root.setOnClickListener(this)
            binding.root.setOnLongClickListener(this)
        }

        override fun onClick(v: View?) {
            clickListener.onClick(adapterPosition)
        }

        override fun onLongClick(v: View?): Boolean {
            clickListener.onLongClick(adapterPosition)
            return true
        }
    }

    inner class NotFoundMonsterViewHolder(private val binding: NotFindedMonsterCardViewBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener, View.OnLongClickListener {

        private lateinit var currentNotFoundedMonster: MonstersModel
        fun bind() {
            currentNotFoundedMonster = monstersList[adapterPosition]
            binding.imIcon.setImageResource(
                R.drawable.ic_monster_not_found
            )
            binding.tvTitle.text = currentNotFoundedMonster.monsterName
            binding.tvDescription.text = currentNotFoundedMonster.monsterDescription

            binding.root.setOnClickListener(this)
            binding.root.setOnLongClickListener(this)
        }

        override fun onClick(v: View?) {
            clickListener.onClick(adapterPosition)
        }

        override fun onLongClick(v: View?): Boolean {
            clickListener.onLongClick(adapterPosition)
            return true
        }
    }
}
package com.vazhasapp.assignment8

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MonstersModel(
    val monsterName: String,
    val monsterDescription: String,
    val monsterIcon: Int? = null,
): Parcelable

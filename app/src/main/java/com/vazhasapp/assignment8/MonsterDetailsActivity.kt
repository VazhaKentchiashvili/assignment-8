package com.vazhasapp.assignment8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.vazhasapp.assignment8.databinding.MonsterDetailsBinding

class MonsterDetailsActivity : AppCompatActivity() {

    private lateinit var binding: MonsterDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_Assignment8)
        binding = MonsterDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()

    }

    private fun init() {
        val myMonster = intent.getParcelableExtra<MonstersModel>(MainActivity.DETAILS_INTENT)
        binding.apply {
            tvTitle.text = myMonster?.monsterName.toString()
            tvDescription.text = myMonster?.monsterDescription.toString()
            imMonster.setImageResource(myMonster?.monsterIcon ?: R.drawable.ic_monster_not_found)
        }
    }
}
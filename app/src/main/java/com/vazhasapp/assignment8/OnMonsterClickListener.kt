package com.vazhasapp.assignment8

interface OnMonsterClickListener {
    fun onClick(position: Int)

    fun onLongClick(position: Int)
}